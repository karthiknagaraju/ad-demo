/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { RouterModule,Router,Routes} from '@angular/router';
import {Location} from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MdSnackBar} from '@angular/material';
import { payerpageservService } from '../services/payerpageserv/payerpageserv.service';

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-payerpage',
    templateUrl: './payerpage.template.html'
})

export class payerpageComponent implements OnInit 
{
  
     banks:any = [];
     branches:any = [];

   	form_status:any;
  
  	titles= new Set(["Mr","Mrs","Miss","Dr","Prof","Chief"]);
    relations= new Set(["Aunt","Brother","Brother-In-Law","Child","Cousin","Doughter","Sister","Father","Mother","Son"]);
    acctypes= new Set(["Saving","Current","Checking","Electronic"]);
  
    //getting first page details for checkbox action...
    m_details=JSON.parse(localStorage.getItem('m_details'));
    slider=this.m_details.slider;
  
    //setting default values for payer for back button..
    p_details:any;
    
  	
  
    //assigning Default values....
    pcheckbox=false;
	ptitle="";
  	pname="";
  	psname="";
    pid="";
  	pmobileno="";
  	phomeno="";
    pworkno="";
    email="";
  	paddrs="";
  	psuburb="";
  	ptown="";
  	ppcode="";
  	prelation="";
  	pbank="";
    pbranch="";
    pacctype="";
    paccno="";
  
  	
   checked=false;
  
   
  constructor(private _router:Router,public _snackbar:MdSnackBar,private _location:Location,private _payerserv:payerpageservService) 
  {
    
     //when u click back button old data will be loaded....
     if(localStorage.getItem('payer_details'))
     {
         this.p_details=JSON.parse(localStorage.getItem('payer_details'));
       
       	  //assigning old values from local storage.....
          this.pcheckbox=this.p_details.pcheckbox;
       	  this.checked=this.p_details.pcheckbox;
          this.ptitle=this.p_details.ptitle;
          this.pname=this.p_details.pname;
          this.psname=this.p_details.psname;
          this.pid=this.p_details.pid;
          this.pmobileno=this.p_details.pmobileno;
          this.phomeno=this.p_details.phomeno;
          this.pworkno=this.p_details.pworkno;
          this.email=this.p_details.email;
          this.paddrs=this.p_details.paddrs;
          this.psuburb=this.p_details.psuburb;
          this.ptown=this.p_details.ptown;
          this.ppcode=this.p_details.ppcode;
          this.prelation=this.p_details.prelation;
       	  this.pbank=this.p_details.pbank;
          this.pbranch=this.p_details.pbranch;
       	  this.pacctype=this.p_details.pacctype;
          this.paccno=this.p_details.paccno;
          this.slider=this.p_details.slider;

     }
    
    //getting banks from database...
    _payerserv.getBanks()
    .subscribe((res) => 
      {
                for(let bank of res)
                {
                        this.banks.push(bank.bname);
                }
      });
    
    //getting branches from database...
    _payerserv.getBranches()
    .subscribe((res) => 
      {
                for(let branch of res)
                {
                        this.branches.push(branch.bbranch);
                }
      });
    
  }
  
  payerpageForm(t)
  {
   this.form_status=t;
  }
  
  addPayer(payerdata)
  {
     if(this.form_status)
    {
        localStorage.setItem('payer_details', JSON.stringify(payerdata));
        this.m_details.slider=payerdata.slider;
        localStorage.setItem('m_details', JSON.stringify(this.m_details));
      
    	this._snackbar.open("Payer Details Succesfully Saved", "close", {  duration: 2000, });
   	 	this._router.navigate(['home/policyconfirm']);
    }
    else
    {
       this._snackbar.open("Please Fill All  the Required Field Properly & Try Again!", "close", {  duration: 2000, });
    }
  }
  
  handleCheckbox(t)
  {
    
    this.checked=! this.checked;
    if(this.checked)
    {
    this.ptitle=this.m_details.title;
    this.pname=this.m_details.cname;
    this.psname=this.m_details.sname;
    this.pid=this.m_details.id;
    this.pmobileno=this.m_details.mnumber;
  	this.phomeno=this.m_details.hnumber;
    this.pworkno=this.m_details.wnumber;
    this.email=this.m_details.email;
  	this.paddrs=this.m_details.address;
  	this.psuburb=this.m_details.suburn;
  	this.ptown=this.m_details.city;
  	this.ppcode=this.m_details.pcode;
    }
    else
    {
     this.ptitle="";
     this.pname="";
  	 this.psname="";
     this.pid="";
  	 this.pmobileno="";
  	 this.phomeno="";
     this.pworkno="";
     this.email="";
  	 this.paddrs="";
  	 this.psuburb="";
  	 this.ptown="";
  	 this.ppcode="";
    }
  }
  
    ngOnInit() 
    {

    }

}
