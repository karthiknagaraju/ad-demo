/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { RouterModule,Router,Routes} from '@angular/router';
import {Location} from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MdSnackBar} from '@angular/material';


//import{ MdetailsComponent } from '../mdetailsComponent/mdetails.component';

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-addmembers',
    templateUrl: './addmembers.template.html'
})

export class addmembersComponent implements OnInit
{
  
    titles= new Set(["Mr","Mrs","Miss","Dr","Prof","Chief"]);
    genders= new Set(["Male","Female"]);
    relations= new Set(["Aunt","Brother","Brother-In-Law","Child","Cousin","Doughter","Sister","Father","Mother","Son"]);
  
    spouses:any={"stitle":"","sname":"","name":"","gender":"","sid":'',"sdob":""};
 
    children:Array<any>=[];
    extmem:Array<any>=[];

  	spouseflag=false;
  	spousebtn=" Add Spouse ";
  	spousebtnicon="add";
  
  	childflag=false
  	extflag=false;
  
  
  
    constructor(private _router:Router,private _location:Location,public _snackbar:MdSnackBar) 
    { 
      
     //when u click back button from next page old data will be loaded....
     if(localStorage.getItem('spouse_details'))
     {
          this.spouses=localStorage.getItem("spouse_details")
      	  this.spouses=JSON.parse(this.spouses);
     }
      
      if(localStorage.getItem('child_details'))
      {
         this.children=JSON.parse(localStorage.getItem('child_details'));
      }
      
      if(localStorage.getItem('extmem_details'))
      {
         this.extmem=JSON.parse(localStorage.getItem('extmem_details'));
      }
      
    }
  
  
  //methods for hidden and view.......
    spouseadd()
    {
      this.spouseflag=!this.spouseflag;
      if(!this.spouseflag)
       {
         		this.spousebtnicon="add";
         		this.spousebtn=" Add Spouse ";
         
         		//resetting local storage......
             	this.spouses={"stitle":"","sname":"","name":"","gender":"","sid":'',"sdob":""};
               	localStorage.removeItem("spouse_details");
                this._snackbar.open("Spouse Data Succesfully Removed", "close", {  duration: 2000, });
       }
      else
      {
                 this.spousebtnicon="highlight_off";
        		this.spousebtn=" Remove Spouse ";
      }
    }
  
  	childadd()
    {
      this.childflag=!this.childflag;
    }
  
  	extadd()
    {
      this.extflag=!this.extflag;
    }

  
  	//methods for add/remove spouse data .......
    addSpouse(spousedata)
    {
      	  localStorage.removeItem("spouse_details");
          localStorage.setItem('spouse_details', JSON.stringify(spousedata));
          this._snackbar.open("Spouse Data Succesfully Added", "close", {  duration: 2000, });
          this.spouses=localStorage.getItem("spouse_details")
      	  this.spouses=JSON.parse(this.spouses);
    }
  
  
    	//methods for add/remove child data .......
      addChild(childdata)
    {
      if(this.children.length<=2)
      {
        this.children.push(childdata);
        localStorage.setItem('child_details', JSON.stringify(this.children));
        this._snackbar.open("Child Data Succesfully Added", "close", {  duration: 2000, });
      }
      else
      {
           this._snackbar.open("Maximum Limit Reached!", "close", {  duration: 2000, });
      }
    }
  
  
  	removeChild(del_index:any)
  	{
       this.children.splice(del_index,1);
       localStorage.setItem('child_details', JSON.stringify(this.children));
       this._snackbar.open("Child Data Succesfully Removed", "close", {  duration: 2000, });
    }
  
      //methods for add/remove  extended member data .......
      addExt(extmemdata)
      {
         if(this.extmem.length<=7)
      	 {
            this.extmem.push(extmemdata);
            localStorage.setItem('extmem_details', JSON.stringify(this.extmem));
            this._snackbar.open("New Member Data Succesfully Saved", "close", {  duration: 2000, });
       	 }
         else
         {
             this._snackbar.open("Maximum Limit Reached!", "close", {  duration: 2000, });
        }
      }
  
  	removeExt(del_index)
  	{
       this.extmem.splice(del_index,1);
       localStorage.setItem('extmem_details', JSON.stringify(this.extmem));
       this._snackbar.open("New Member Data Succesfully Removed", "close", {  duration: 2000, });
    }
  
  
  //final page submit.....
  addMembers()
  {					
    if( (window.localStorage.spouse_details !== undefined) || (window.localStorage.child_details !== undefined) || (window.localStorage.extmem_details !== undefined) )
  	{
         this._snackbar.open("Additional Members Details Succesfully Saved", "close", {  duration: 2000, });
    	 this._router.navigate(['home/payerpage']);
   	}
   	else
    {
           this._snackbar.open("Please Fill Atleast One Additional Member Details & Try Again!", "close", {  duration: 2000, });      
    }
  }
  
  
    ngOnInit()
    {

    }

}
