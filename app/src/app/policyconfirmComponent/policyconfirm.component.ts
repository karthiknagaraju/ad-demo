/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { RouterModule,Router,Routes} from '@angular/router';
import {Location} from '@angular/common';
import { FormsModule } from '@angular/forms';
import {MdSnackBar} from '@angular/material';
import { pconfirmservService } from '../services/pconfirmserv/pconfirmserv.service';

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-policyconfirm',
    templateUrl: './policyconfirm.template.html'
})

export class policyconfirmComponent implements OnInit 
{
  
   //getting first page details...
    p_details=JSON.parse(localStorage.getItem('payer_details'));
    slider=this.p_details.slider;
  	email=this.p_details.email;
  	pname=this.p_details.pname;
  	psname=this.p_details.psname;
  
    clientdata:any={"mdetails":"","sdetails":"","cdetails":"","extdetails":"","pdetails":""};
  
   constructor(private _router:Router,private _location:Location,public _snackbar:MdSnackBar,private _pconfirmserv:pconfirmservService) 
  { 
     _snackbar.open("Please Verify & Confirm Your Details !", "close", {  duration: 2000, });
    
  }
  
  finalSubmit(final_details)
  {
    if(final_details.agree)
    {
      	this.clientdata.mdetails=JSON.parse(localStorage.getItem('m_details'));
      	this.clientdata.sdetails=JSON.parse(localStorage.getItem('spouse_details'));
      	this.clientdata.cdetails=JSON.parse(localStorage.getItem('child_details'));
      	this.clientdata.extdetails=JSON.parse(localStorage.getItem('extmem_details'));
      	this.clientdata.pdetails=JSON.parse(localStorage.getItem('payer_details'));

 		//inserting data to database.......
        this._pconfirmserv.submitData(this.clientdata).subscribe();

   		this._snackbar.open("Your Details are Successfully Submited, Thank You.", "close", {  duration: 2000, });
       	this._router.navigate(['home/compleated']);
    }
    else
    {
       this._snackbar.open("Please Agree Terms & Conditions ! ", "close", {  duration: 2000, });
    }
  }
  
  
    ngOnInit() 
  	{

    }

}
