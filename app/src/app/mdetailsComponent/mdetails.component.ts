/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'
import { RouterModule,Router,Routes} from '@angular/router';
import {MdSnackBar} from '@angular/material';
import { FormsModule } from '@angular/forms';
import { mdetailsservService } from '../services/mdetailsserv/mdetailsserv.service';



/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-mdetails',
    templateUrl: './mdetails.template.html'
})

export class mdetailsComponent implements OnInit
{
    
  occupations:any = [];

  titles= new Set(["Mr","Mrs","Miss","Dr","Prof","Chief"]);
  mincomes= new Set(["No Income","Up To 3000","3,000-10,000","10,000-15,000","15,000-20,000","25,000-30,000","30,000-35,000","Above 35,000","Not Disclosed"])
  edus= new Set(["No Matric","Matric","Univercity, Technicon, Degree, Diploma","Post-Graduate (Masters etc..)"]);
  
  

  //assigning default values.....
    title="";
  	cname="";
  	sname="";
    id="";
  	mnumber="";
  	hnumber="";
    wnumber="";
    email="";
  	address="";
  	suburn="";
  	city="";
  	pcode="";
  	income="";
    occu="";
    edu="";
    slider="10000";
  
  form_status:any;
  m_details:any;
  
  
  
  constructor(private _router:Router,public _snackbar:MdSnackBar,private _mdserv:mdetailsservService) 
  { 
    //when u click back button old data will be loaded....
     if(localStorage.getItem('m_details'))
     {
         this.m_details=JSON.parse(localStorage.getItem('m_details'));
       
       //assigning old values from local storage.....
          this.title=this.m_details.title;
          this.cname=this.m_details.cname;
          this.sname=this.m_details.sname;
          this.id=this.m_details.id;
          this.mnumber=this.m_details.mnumber;
          this.hnumber=this.m_details.hnumber;
          this.wnumber=this.m_details.wnumber;
          this.email=this.m_details.email;
          this.address=this.m_details.address;
          this.suburn=this.m_details.suburn;
          this.city=this.m_details.city;
          this.pcode=this.m_details.pcode;
       	  this.income=this.m_details.income;
          this.occu=this.m_details.occu;
          this.edu=this.m_details.edu;
          this.slider=this.m_details.slider;

     }
    
    
    //getting ocupations from database...
    _mdserv.getOccupations()
    .subscribe((res) => 
      {
                for(let occu of res)
                {
                        this.occupations.push(occu.otitle);
                }
      });
  }
      
  mdetailsForm(t)
  {
   this.form_status=t;
  }
  
  submit_mdetails(m_details)
  {
    if(this.form_status)
    {
      localStorage.setItem('m_details', JSON.stringify(m_details));
      this._snackbar.open("Member Details Succesfully Saved!", "close", {  duration: 2000, });
      this._router.navigate(['home/addmembers']);
    }
    else
    {
       this._snackbar.open("Please Fill All  the Required Field Properly & Try Again!", "close", {  duration: 2000, });
    }
  }
  
  
   ngOnInit() 
   {

   }
  

}


