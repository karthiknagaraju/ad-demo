/*DEFAULT GENERATED TEMPLATE. DO NOT CHANGE SELECTOR TEMPLATE_URL AND CLASS NAME*/
import { Component, OnInit } from '@angular/core'

/**
* Model import Example :
* import { HERO } from '../models/hero.model';
*/

/**
 * Service import Example :
 * import { HeroService } from '../services/hero/hero.service';
 */

@Component({
    selector: 'bh-compleated',
    templateUrl: './compleated.template.html'
})

export class compleatedComponent implements OnInit 
{
  
  constructor()
  {
    localStorage.clear();
  }
  
   ngOnInit() 
  {

  }

}
