import { PageNotFoundComponent } from '../not-found.component';
import { LayoutComponent } from '../layout/layout.component';
import { TestComponent } from '../test/test.component';
import { NotificationService } from '../service/notification.service';
import { LocalStorageService } from '../service/local-storage.service';
import { ImgSrcDirective } from '../directives/imgSrc.directive';
import { BAuthGuard } from '../service/bAuthGuard.service';
import { BAppService } from '../service/bApp.service';
import { BLocalStorageService } from '../service/bLocalStorage.service';
import { BSessionStorage } from '../service/bSessionStorage.service';
import { BLoginService } from '../service/bLogin.service';
import { HttpModule, Http, XHRBackend, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { BHttp } from '../service/bHTTP';
import { BHTTPLoader } from '../service/bHTTPLoader';
import { PubSubService } from '../service/pubSub.service';
import { AlertComponent } from '../alertComponent/alert.component'; 
import { BDataSourceService } from '../service/bDataSource.service';

//CORE_REFERENCE_IMPORTS
//CORE_REFERENCE_IMPORT-compleatedComponent
import { compleatedComponent } from '../compleatedComponent/compleated.component';
//CORE_REFERENCE_IMPORT-pconfirmservService
import { pconfirmservService } from '../services/pconfirmserv/pconfirmserv.service';
//CORE_REFERENCE_IMPORT-payerpageservService
import { payerpageservService } from '../services/payerpageserv/payerpageserv.service';
//CORE_REFERENCE_IMPORT-additionalmemberComponent
import { homeComponent } from '../homeComponent/home.component';
//CORE_REFERENCE_IMPORT-datamodelComponent
import { mdetailsComponent } from '../mdetailsComponent/mdetails.component';
//CORE_REFERENCE_IMPORT-datserviceService
import { mdetailsservService } from '../services/mdetailsserv/mdetailsserv.service';
//CORE_REFERENCE_IMPORT-memberpageComponent
import { addmembersComponent } from '../addmembersComponent/addmembers.component';
//CORE_REFERENCE_IMPORT-clientpageComponent
//CORE_REFERENCE_IMPORT-clientpageComponent
import { payerpageComponent } from '../payerpageComponent/payerpage.component';
//CORE_REFERENCE_IMPORT-clientpageComponent
import { policyconfirmComponent } from '../policyconfirmComponent/policyconfirm.component';



export function httpFactory(backend: XHRBackend, defaultOptions: RequestOptions, bHTTPLoader: BHTTPLoader) {
  return new BHttp(backend, defaultOptions, bHTTPLoader);
}


/**
*bootstrap for @NgModule
*/
export const appBootstrap: any = [
  LayoutComponent,
];

/**
*Entry Components for @NgModule
*/
export const appEntryComponents: any = [
  AlertComponent
];

/**
*declarations for @NgModule
*/
export const appDeclarations = [
  ImgSrcDirective,
  LayoutComponent,
  TestComponent,
  AlertComponent,
  //CORE_REFERENCE_PUSH_TO_DEC_ARRAY
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-compleatedComponent
compleatedComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-additionalmemberComponent
homeComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-datamodelComponent
mdetailsComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-memberpageComponent
addmembersComponent,
//CORE_REFERENCE_PUSH_TO_DEC_ARRAY-clientpageComponent
payerpageComponent,
policyconfirmComponent,
  PageNotFoundComponent
];

/**
* provider for @NgModuke
*/
export const appProviders = [
  {
    provide: Http,
    useFactory: httpFactory,
    deps: [XHRBackend, RequestOptions, BHTTPLoader]
  },
  NotificationService,
  BAuthGuard,
  //CORE_REFERENCE_PUSH_TO_PRO_ARRAY
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-pconfirmservService
pconfirmservService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-payerpageservService
payerpageservService,
//CORE_REFERENCE_PUSH_TO_PRO_ARRAY-datserviceService
mdetailsservService,
  LocalStorageService,
  PubSubService,
  BLoginService,
  BSessionStorage,
  BLocalStorageService,
  BAppService,
  BHTTPLoader,
  BDataSourceService

];

/**
* Routes available for bApp
*/

// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_START
export const appRoutes = [{path: 'home', component: homeComponent,
children: [{path: 'mdetails', component: mdetailsComponent},{path: 'addmembers', component: addmembersComponent},{path: 'payerpage', component: payerpageComponent},{path: 'policyconfirm', component: policyconfirmComponent},{path: 'compleated', component: compleatedComponent}]},{path: '', redirectTo: 'home/mdetails', pathMatch: 'full'},{path: '**', component: PageNotFoundComponent}]
// CORE_REFERENCE_PUSH_TO_ROUTE_ARRAY_END

