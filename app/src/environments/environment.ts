export const environment = {
    "envId": "19534901-59a4-9812-5b87-c1e07d490356",
    "name": "dev",
    "properties": {
        "production": false,
        "baseUrl": "http://localhost:3000",
        "tenantName": "jatahworx",
        "appName": "clientplcy",
        "namespace": "com.neutrinos.jatahworx.clientplcy",
        "isNotificationEnabled": false,
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID"
    }
}