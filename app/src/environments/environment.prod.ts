export const environment = {
    "envId": "6b8a3d7e-0dcb-cef2-f14f-56e965a50d26",
    "name": "prod",
    "properties": {
        "production": true,
        "baseUrl": "http://localhost:3000",
        "tenantName": "jatahworx",
        "appName": "clientplcy",
        "namespace": "com.neutrinos.jatahworx.clientplcy",
        "isNotificationEnabled": false,
        "firebaseSenderId": "FIREBASE_SENDER_ID",
        "firebaseAuthKey": "FIREBASE_AUTH_KEY",
        "authDomain": "FIREBASE_AUTH_DOMAIN",
        "databaseURL": "FIREBASE_DATABASE_URL",
        "storageBucket": "FIREBASE_STORAGE_BUCKET",
        "messagingSenderId": "FIREBASE_SENDER_ID"
    }
}